
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user
 */
public class MainOX {

    char[][] board = new char[3][3];
    boolean win = false;

    public void showboardO() {
        System.out.println("Welcome to OX Game");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void showboard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void drawboard(char symbol, int row, int col) {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (row - 1 == r && col - 1 == c) {
                    board[r][c] = symbol;
                }
            }
        }
        showboard();
    }

    int status = 0;
    char symbol = ' ';

    public void turn() {
        Scanner kb = new Scanner(System.in);
        for (int i = 0; i < 9; i++) { //ต้องมี 9 รอบ แต่ทดสอบแค่ 3 รอบ
            if (status % 2 == 0) {
                System.out.println("Turn O");
                symbol = 'O';
            } else {
                System.out.println("Turn X");
                symbol = 'X';
            }
            int row, col;
            System.out.println("Please input row, col: ");
            row = kb.nextInt();
            col = kb.nextInt();

            if (row > 0 && col > 0 && row <= 3 && col <= 3 && board[row - 1][col - 1] == '-') {
                drawboard(symbol, row, col);
            } else {
                System.out.println("***Please input row, col again***");
                turn();
            }

            status++;

            checkwin(symbol);
            if (win == true) {
                System.out.println(">>> " + symbol + " Win <<<");
                break;
            } else if (i == 8) {
                System.out.println(">>> Draw <<< ");
                break;
            }
        }
    }

    public void checkwin(char symbol) {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                //checkcol                        
                if (board[0][c] == symbol && board[0][c] == board[1][c] && board[0][c] == board[2][c]) {
                    win = true;
                    break;
                } //checkrow
                else if (board[r][0] == symbol && board[r][0] == board[r][1] && board[r][0] == board[r][2]) {
                    win = true;
                    break;
                } //checkdiagonal
                else if (board[0][0] == symbol && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
                    win = true;
                    break;
                } else if (board[0][2] == symbol && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
                    win = true;
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        MainOX ox = new MainOX();
        ox.showboardO();
        ox.turn();
    }

}
